package client.simple;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	
	public void connect() throws UnknownHostException, IOException {
		
		Socket socket = new Socket("localhost", 1989);
		
		//sending to server
		PrintStream printStream = new PrintStream(socket.getOutputStream());
		printStream.println("Study and share");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		
		String response = br.readLine();
		if (response != null) {
			System.out.println("Data response from server: " + response);
			socket.close();
		}
		
	}
	
	public static void main(String[] args) throws UnknownHostException, IOException {
		Client client = new Client();
		client.connect();
	}
}
